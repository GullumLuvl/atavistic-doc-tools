/* Automatically generate slideshows from *lists* contained in DIV of class "slideshow".
 *
 * Copyright Guillaume LOUVEL 2018/07/12
 * 
 * Features:
 * - show all images normally if javascript can't be loaded.
 * - no autoplay, only control buttons to change images.
 * - a "toggle" button to alternate between the normal view and the slideshow view.
 * - as many slideshows as wanted per page.
 *
 * In order to work, it requires the following css code:
 * 
 *    .slideshow > ul, .slideshow > ol {
 *        position: relative;
 *        list-style-type: none;
 *    }
 *
 *    li.slide {
 *        position: absolute;
 *        opacity: 0;
 *        z-index: 1;
 *    }
 *
 *    li.showing {
 *        position: static;
 *        left: 0px;
 *        top: 0px;
 *        width: 100%;
 *        height: 100%;
 *        opacity: 1;
 *        z-index: 2;
 *    }
 *
 *    .slideshow > button.controls {
 *        position: static;
 *    }
 *
 * Code inspired from this tutorial:
 *     https://www.sitepoint.com/make-a-simple-javascript-slideshow-without-jquery/
 *
 */

"use strict";

//var message = document.getElementById("message");
var loaded_myslideshow = -1;

var debug = 1;
var log = function (){};
if (debug) {
    log = console.log;
}

function sleep(ms) {
  return new Promise(resolve => setTimeout(resolve, ms));
}
function oldsleep(ms) {
  const date = Date.now();
  let currentDate = null;
  do {
    currentDate = Date.now();
  } while (currentDate - date < ms);
}

function makeSlideshow(container) {
    //var slides = container.querySelectorAll('.slideshow>ul>li, .slideshow>ol>li, .slideshow>figure');
    //var slides = container.querySelector('li, figure');
    var slides = [];
    log("container.childNodes.length:"+container.childNodes.length);
    for (let i=0; i<container.childNodes.length; i++) {
        let child = container.childNodes[i];
        log("Got child: " + child);
        if (child.tagName == "FIGURE") {
            log("has tag: "+child.tagName);
            slides.push(child) ;
        } else if ((child.tagName == "UL") || (child.tagName == "OL")) {
            log("List of: " + child.childNodes.length);
            for (let j=0; j<child.childNodes.length; j++) {
                if (child.childNodes[j].tagName == "LI" || child.childNodes[j].tagName == "FIGURE") {
                    slides.push(child.childNodes[j]) ;
                    log("child.child has tag: "+child.childNodes[j].tagName);
                }
            }
        }
    }
    
    log("get slide elements: " + slides.length);
    //log(typeof slides);
    log(slides);
    var currentSlide = 0;
    var slideshow_on = false;
    var sliding = false;  // when button is continuously pressed

    function goToSlide(n) {
        slides[currentSlide].className = 'slide';
        slides[currentSlide].onclick = undefined;
        currentSlide = (n+slides.length) % slides.length;
        slides[currentSlide].className = 'slide showing';
        slides[currentSlide].onclick = nextSlide;
        slideNumberElement.innerHTML = String(currentSlide+1) + "/" + slides.length;
    }

    function nextSlide() {
        goToSlide(currentSlide+1);
        //message.innerHTML += "<br>Next!";
    }

    function previousSlide() {
        goToSlide(currentSlide-1);
        //message.innerHTML += "<br>Previous!";
    }

    async function repeatNextSlide() {
        sliding = true;
        //nextSlide();
        var slid = 0;
        while ( sliding ) {
            nextSlide();
            await sleep(slid++<4?300:50);
            //oldsleep(300);
        }
    }
    async function repeatPreviousSlide() {
        sliding = true;
        var slid = 0;
        while ( sliding ) {
            previousSlide();
            await sleep(slid++<4?300:50);
        }
    }
    function stopSliding() {
        sliding = false;
    }
    
    // Create control buttons and slide indicator
    var toStart = document.createElement("button");
    toStart.innerHTML = "|&lt;&lt;";
    toStart.className = "controls";
    var previous = document.createElement("button");
    var previoustext = document.createTextNode("<");
    previous.appendChild(previoustext);
    previous.className = "controls";

    var slideNumberElement = document.createElement("span");
    slideNumberElement.appendChild(
            document.createTextNode(currentSlide+1 + "/" + slides.length));
    slideNumberElement.className = "controls";

    var next = document.createElement("button");
    var nexttext = document.createTextNode(">");
    next.appendChild(nexttext);
    next.className = "controls";
    //previous.onclick = previousSlide;
    //next.onclick = nextSlide;
    previous.onmousedown = repeatPreviousSlide;
    next.onmousedown = repeatNextSlide;
    previous.onmouseup = stopSliding;
    next.onmouseup = stopSliding;
    toStart.onclick = function(){goToSlide(0);};

    function toggle() {
        var i;
        if ( !slideshow_on ) {
            // Setup slideshow with css styling by adding the right class:
            for (i=0; i<slides.length; i++) {
                slides[i].className = "slide";
            }
            slides[currentSlide].className += " showing";
            previous.style.visibility = "visible";
            toStart.style.visibility = "visible";
            slideNumberElement.style.visibility = "visible";
            next.style.visibility = "visible";
            slideshow_on = true;
        } else {
            // Remove the class name to inhibit styling (see css).
            for (i=0; i < slides.length; i++) {
                slides[i].className = '';
            }
            toStart.style.visibility = "hidden";
            previous.style.visibility = "hidden";
            slideNumberElement.style.visibility = "hidden";
            next.style.visibility = "hidden";
            slideshow_on = false;
        }
    }

    toggle();

    var togglebutton = document.createElement("button");
    togglebutton.appendChild(document.createTextNode("Slideshow On/Off"));
    togglebutton.onclick = toggle;

    var titlenode = document.createElement('span');
    titlenode.innerHTML = container.title;
    //console.log(titlenode.style);
    titlenode.style.textAlign = 'center';
    titlenode.style.fontWeight = 'bold';
    titlenode.style.padding = '0 15px';

    var slideshowControlBar = document.createElement("span");
    slideshowControlBar.appendChild(titlenode);
    slideshowControlBar.appendChild(togglebutton);
    slideshowControlBar.appendChild(toStart);
    slideshowControlBar.appendChild(previous);
    slideshowControlBar.appendChild(slideNumberElement);
    slideshowControlBar.appendChild(next);

    container.insertBefore(slideshowControlBar, container.firstChild);
    //container.className += " loaded";

}

function makeAllSlideshows() {
    //log('Making slideshows...');
    var slideshows = document.getElementsByClassName('slideshow');
    var i;
    for (i=0; i<slideshows.length; i++) {
        makeSlideshow(slideshows[i]);
    }
    log('Made ' + i + ' slideshows');
    loaded_myslideshow = i;
}

log('Defined myslideshow functions. Ready.');

document.body.onload = makeAllSlideshows;
//document.addEventListener("DOMContentLoaded", makeAllSlideshows);
//window.onbeforeunload = makeAllSlideshows;
//     function() {
//     document.getElementById("user-greeting").textContent = "Welcome back, Bart";
//     });
// this function runs when the DOM is ready, i.e. when the document has been parsed

//message.innerHTML = "HEY";
